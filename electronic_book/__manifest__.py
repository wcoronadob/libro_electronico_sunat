{
    'name': "electronic_book",

    'summary': """
        Libro Electronico""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'report_xlsx',
        'stock',
        'account',
        'base',

    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'report/report.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/account_move_inherit.xml',
        'wizard/report_by_state_book_view_wizard.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
# -*- coding: utf-8 -*-
