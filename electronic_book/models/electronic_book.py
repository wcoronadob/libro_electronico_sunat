# -*- coding: utf-8 -*-

from odoo import models, fields, api


class electronic_book(models.Model):
    _name = 'electronic_book.electronic_book'
    _description = 'Tipo de Libro Electronico'

    name = fields.Char(string='Nombre')
    book_code = fields.Char(string='Código')
    is_active = fields.Boolean(string='Activo')
    account_move_ids = fields.Many2one('account.move', string='cuenta')


