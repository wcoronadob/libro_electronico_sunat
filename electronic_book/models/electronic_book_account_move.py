# -*- coding: utf-8 -*-

from odoo import fields, models, api


class electronic_book_account_move(models.Model):
    _inherit = 'account.move'
    _description = 'Herencia del account.move'

    electronic_book_ids = fields.One2many(
        comodel_name='electronic_book.electronic_book',
        inverse_name='account_move_ids',
        string='Libros Electrónicos',
        required=False)
