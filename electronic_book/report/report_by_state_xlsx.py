from odoo import fields, models, api


class ReportByStateXLS(models.AbstractModel):
    _name = 'report.electronic_book.report_by_state_excel'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, books):

        worksheet = workbook.add_worksheet('Libros Electronicos')

        worksheet.set_column('B:B', 60)
        worksheet.set_column('C:C', 40)
        worksheet.set_column('D:D', 40)
        worksheet.set_row(1, 40)
        worksheet.set_row(2, 30)

        merge_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': 'yellow'})

        result_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'font_size': 18,
            'align': 'center',
            'valign': 'vcenter'
        })

        subtitle_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'font_size': 11,
            'align': 'center',
            'valign': 'vcenter'
        })

        data_format = workbook.add_format({
            'border': 1,
            'font_size': 12,
        })

        worksheet.merge_range('B2:D2', 'Reporte de Libros Electrónicos Registrados', merge_format)
        worksheet.merge_range('B3:C3', 'Estado de Libros', merge_format)
        worksheet.write('D3', data['form_data']['scope'], result_format)

        worksheet.write('B4', 'Nombre del Libro', subtitle_format)
        worksheet.write('C4', 'Código', subtitle_format)
        worksheet.write('D4', 'Estado', subtitle_format)

        fila = 3
        columna = 1

        for book in data['books']:
            fila += 1
            worksheet.write(fila, columna, book['name'], data_format)
            worksheet.write(fila, columna + 1, book['book_code'], data_format)
            worksheet.write(fila, columna + 2, book['is_active'], data_format)






