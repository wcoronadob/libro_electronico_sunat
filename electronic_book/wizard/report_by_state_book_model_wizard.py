# -*- coding: utf-8 -*-

from odoo import fields, models, api


class electronic_book_wizard(models.TransientModel):
    _name = 'electronic_book.report_by_state'
    _description = 'Modelo que me va a permitir generar los reportes por estado.'

    scope = fields.Selection(selection=[
        ('todos', 'Todos'),
        ('inactivos', 'Inactivos'),
        ('activos', 'Activos'),
        ],
        string="Alcance",
        default="todos")

    def action_report_by_state(self):

        domain = []

        scope = self.scope

        if scope == 'inactivos':
            domain += [('is_active', '=', False)]

        if scope == 'activos':
            domain += [('is_active', '=', True)]

        books = self.env['electronic_book.electronic_book'].search_read(domain)

        data = {
            'form_data': self.read()[0],
            'books': books
        }
        return self.env.ref('electronic_book.report_by_state_in_excel').report_action(self, data=data)
